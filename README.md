# README #

### What is this repository for? ###

* I'm messing about... Making a Discord robot, having some fun

### How do I get set up? ###

* Make sure you have node, at least v4.0.0 (as Discordie requires)
* Clone the repo and run `npm install`
* Change the variables in `.example.env` and save it as `.env`
* Boot the thing up by running `node index.js`