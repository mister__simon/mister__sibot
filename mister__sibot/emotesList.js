var config = require('./config'),
	style = require('../lib/logStyles'),
	request = require('request'),
	fs = require('fs'),
	path = require('path');

function getTwitchEmotesList(onSuccess, onError){
	var req = request('https://twitchemotes.com/api_cache/v2/global.json', (error, response, body) => {
		if (!error && response.statusCode == 200) {
			var responseJSON = JSON.parse(body),
				template = responseJSON.template.small,
				emoteURLs = [],
				cur;

			for(var trigger in responseJSON.emotes){
				cur = responseJSON.emotes[trigger];
				emoteURLs.push({
					trigger: trigger,
					url: template.replace('{image_id}', cur.image_id)
				});
			}

			onSuccess(emoteURLs);
		} else {
			console.log(style.error("Could not get Twitch Emotes list"));
			onError();
		}
	});
}

function createCacheableRequest(emoteListItem, onError, onEnd){
	request(emoteListItem.url)
		.on('error', onError)
		.on('response', response => {
			var path = config.emotes_cache,
				filename;

			if(emoteListItem.filename != undefined){
				filename = emoteListItem.filename;
			} else {
				filename = emoteListItem.trigger + "." + (response.headers['content-type'] ? response.headers['content-type'].slice(-3) : 'png');
			}

			response.pipe(
				fs.createWriteStream(path + '/' + filename)
					.on('error', err => {
						console.log(style.error(err));
					})
			);

			response.resume();
		})
		.on('end', onEnd);
}

function cacheEmotesList(emotesList, callback){
	if(!emotesList){
		return false;
	}

	var toCache = emotesList.length;

	for (var i = 0; i < emotesList.length; i++) {
		createCacheableRequest(emotesList[i],
			error => {
				toCache -= 1;
				console.log(style.error(error));
			}, () => {
				console.log(toCache);
				if(--toCache === 0){
					callback();
				}
			}
		);
	}
}

function cacheTwitchEmotes(callback){
	getTwitchEmotesList(twitchEmotesList => {
		console.log(style.success("Got Twitch Emotes List"));

		cacheEmotesList(twitchEmotesList, () => {
			console.log(style.success("Cached all the twitch emotes"));
			callback();
		});

	}, () => {
		console.log(style.error("Failed to get Twitch Emotes"));
	});
}

function createCacheFile(){
	var cacheFileLocation = path.join(__dirname, 'emotes'),
		contents = fs.readdirSync(config.emotes_cache),
		cacheFileContents = contents.map(filename => {
			return {
				trigger: path.parse(filename).name,
				path: config.emotes_cache + "/" + filename
			};
		});

	return fs.writeFileSync(config.emotes_cache + ".json", JSON.stringify(cacheFileContents));
}

var cachedEmotes;
function getEmotes(updateCache){
	if(updateCache){
		createCacheFile();
	}

	if(!cachedEmotes || updateCache){
		cachedEmotes = JSON.parse(fs.readFileSync(config.emotes_cache + ".json"));
	}
	return cachedEmotes;
}

function cacheEmote(emoteListItem, onSuccess, onError){
	createCacheableRequest(emoteListItem, onError, () => {
		getEmotes(true);
		onSuccess();
	});
}

function removeEmote(emoteName){
	getEmotes(true);

	for (var i = 0, l = cachedEmotes.length; i < l; i++) {
		if(emoteName === cachedEmotes[i].trigger){
			fs.unlinkSync(cachedEmotes[i].path);
			getEmotes(true);
			return true;
		}
	}

	return false;
}

module.exports = {
	cacheTwitchEmotes: cacheTwitchEmotes,
	cacheEmote: cacheEmote,
	getEmotes: getEmotes,
	removeEmote: removeEmote
};