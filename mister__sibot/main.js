var config = require('./config'),
	Discordie = require("discordie"),
	style = require('../lib/logStyles');

console.log(style.welcome("                            ") + style.chatOut("┌───────┐"));
console.log(style.welcome("--------------------------- ") + style.chatOut("│-()--o-│"));
console.log(style.welcome("-------mister__sibot------- ") + style.chatOut("│----*--│"));
console.log(style.welcome("--------------------------- ") + style.chatOut("│-|░░░░|│ < BleepBloop"));
console.log(style.welcome("                            ") + style.chatOut("└───────┘"));

// Initialise Discordie client
var client = new Discordie();

client.connect({ token: config.token });

client.Dispatcher.on("GATEWAY_READY", e => {
	console.log(style.success("Connected as: ") + style.chatMainOut(client.User.username));
});

client.Dispatcher.on("DISCONNECTED", e => {
	console.log(style.error("!------DISCONNECTED------!"));
	console.log(e);
	console.log(style.error("!------DISCONNECTED------!"));
});

client.Dispatcher.on("GATEWAY_RESUMED", e => {
	console.log(style.success("\n\r!--------RESUMED---------!"));
});


// Message prepping
client.Dispatcher.on("MESSAGE_CREATE", e => {
	var msgLower = e.message.content.toLowerCase(),
		mentionedByName = (msgLower.indexOf(client.User.username.toLowerCase()) !== -1 || msgLower.indexOf(config.bot_short_name.toLowerCase()) !== -1),
		mentionedByTag = !!(e.message.mentions.find(mention => {
			return (mention.username === client.User.username);
		}));

	var mentioned = (mentionedByName || mentionedByTag),
		isMe = (e.message.author.username === client.User.username);

	routeMessage({
		mentioned: mentioned,
		isMe: isMe,
		toOwner: (e.message.channel.is_private == true && e.message.channel.name == config.owner)
	}, e);
});

// Message routing
function routeMessage(meta, e){
	if(process.env.BASIC_LOGGING == 1){
		var attachments = [];

		if(e.message.attachments.length){
			attachments = e.message.attachments.map(attachment => {
				return attachment.filename;
			});
		}

		var chat = (meta.isMe ? style.chatOut : style.chatIn),
			chatMain = (meta.isMe ? style.chatMainOut : style.chatMainIn);

		var messageLog = (meta.isMe ? "<-- " : "--> ")
			+ (e.message.channel.name)
			+ " - "
			+ (e.message.channel.guild ? e.message.channel.guild.name + " - " : "")
			+ chatMain(e.message.author.username + ": " + e.message.resolveContent())
			+ chatMain(attachments.length ? " (Attachments: " + attachments.join(', ') + ")" : "");

		console.log(chat(messageLog));
	}

	// Currently no need to respond to yourself
	if(meta.isMe){
		return;
	}

	if(meta.toOwner){
		// Direct messages from bot's owner act as commands
		var lowContent = e.message.content.toLowerCase();

		if(lowContent === "cur"){
			getCurrentChannel(e);
			return;
		}

		if(lowContent.indexOf("join") !== -1){
			joinChannel(e);
			return;
		}

		if(lowContent.indexOf("say ") !== -1){
			sayChannel(e);
			return;
		}

		if(lowContent === "listemotes"){
			listEmotes(e);
			return;
		}

		if(lowContent === "cacheemotes"){
			cacheEmotes(e);
			return;
		}

		if(lowContent === "addemote"){
			addEmote(e);
			return;
		}

		if(lowContent.indexOf("rmvemote ") !== -1){
			rmvEmote(e);
			return;
		}

		if(lowContent === "avatar"){
			setAvatar(e);
			return;
		}

		if(lowContent.indexOf("game") !== -1){
			setGame(e);
			return;
		}

		if(lowContent === "help"){
			e.message.reply(
				"Commands are:\n\n" +
				"join <index>\n - Provides a list of rooms, or joins one of them\n\n" +
				"say <message>\n- Say something to joined room\n\n" +
				"cur\n - Current room to talk in\n\n" +
				"cacheemotes\n - Cache emotes in the emotes directory\n\n" +
				"listemotes\n - Returns a giant list of emotes\n\n" +
				"addemote [attachment]\n - Attach a PNG image, add it to the bot\n\n" +
				"rmvemote <emotename>\n - Attach a PNG image, add it to the bot\n\n" +
				"avatar [attachment]\n - Attach an image, set the bot's avatar\n\n" +
				"game <game>\n - Set the bot's game status. Leave blank to reset to nothing."
			);
			return;
		}
	}

	// Check emotes
	checkEmotes(e);

	if(!meta.toOwner) {
		if(meta.mentioned){
			beACleverBot(e);
			return;
		}
	}

}

// Join a channel - for use with the "say" command
var activeChannel;
function joinChannel(e){
	var commandParam = e.message.content.substring("join ".length),
		channels = client.Channels.filter(c => c.type == 'text')
					.concat(client.DirectMessageChannels.toArray());

	if(!commandParam){
		// Generate a list of joinable channels
		var channel,
			message = [];
		for (var i = 0, l = channels.length; i < l; i++) {
			channel = channels[i];
			message.push("["+i+"] " + channel.name + (channel.guild ? " on " + channel.guild.name : ""));
		}
		e.message.reply(message.join("\n"));
	} else {
		// Join a channel
		activeChannel = channels[commandParam];
		getCurrentChannel(e);
	}
}

// Output a message about what channel the "say" command will use
function getCurrentChannel(e){
	if(!activeChannel){
		e.message.reply("No channel selected. Search for one using \"join\"");
		return;
	} else {
		e.message.reply("Joined " + activeChannel.name + (activeChannel.guild ? " on " + activeChannel.guild.name : ""));
	}
}

// The bot will relay a message to the selected activeChannel
function sayChannel(e){
	if(!activeChannel){
		e.message.reply("No channel selected. Search for one using \"join\"");
		return;
	} else {
		var commandParam = e.message.content.substring("say ".length);

		if(commandParam.length){
			activeChannel.sendMessage(commandParam);
			e.message.reply("I said '"+commandParam+"'");
		} else {
			e.message.reply("You didn't actually give me anything to say...");
			e.message.reply("The command is 'say <some text to relay to joined channel>'");
		}
	}
}

// Cleverbot stuff
var cleverbot = require("cleverbot.io"),
	bot = new cleverbot(process.env.CLEVERBOT_USER, process.env.CLEVERBOT_KEY, "someunusualnickname"),
	botCreated = false,
	botSession;

function beACleverBot(e){
	if(!botCreated){
		bot.create((err, session) => {
			if(!err){
				console.log(style.success("Cleverbot session: " + session));
				botSession = session;
			} else {
				console.log(style.error("Cleverbot session: ERRORED!"));
			}

			botCreated = true;
			beACleverBot(e);
		});
	} else {
		var noResponse = "My little robot brain doesn't understand right now. Beep boop.";
		if(botSession){
			bot.setNick(botSession);
			bot.ask(e.message.content, (err, response) => {
				if(err){
					e.message.reply(noResponse);
					console.log(style.error("ERROR"), err, response);
				} else {
					e.message.reply(response);
				}
			});
		} else {
			e.message.reply(noResponse);
		}
	}
}

// Check for emotes + return appropriate links
var emotesList = require('./emotesList');

function checkEmotes(e){
	var emotes = emotesList.getEmotes();
	
	for (var i = 0, l = emotes.length; i < l; i++) {
		if(e.message.content.indexOf(emotes[i].trigger) !== -1){
			e.message.channel.uploadFile(emotes[i].path);
		}
	}
}

function cacheEmotes(e){
	emotesList.getEmotes(true);
	e.message.reply("Emotes were cached");
}

function listEmotes(e){
	var emotes = emotesList.getEmotes();
	if(emotes.length === 0){
		e.message.reply("No emotes.");		
	} else {
		// Format everything into
		// \nCharacter ---- lots - of - emote - trigger - names
		var previousChar = "";
		var emotesResponse = emotes.reduce(
			function(carry, emote){
				if(emote.trigger[0].toUpperCase() !== previousChar){
					previousChar = emote.trigger[0].toUpperCase();
					carry += "\n" + previousChar + " ---- ";
				}

				return carry + emote.trigger + " - ";
			},
			""
		);

		// Split on the new lines (\n) iterate through and break long lines into shorter ones
		var responseLines = emotesResponse.split('\n');
		var chunkedResponseLines = [];

		for (var i = 0; i < responseLines.length; i++) {
			if(responseLines[i].length === 0) continue;
			chunkedResponseLines = chunkedResponseLines.concat(responseLines[i].match(/.{1,1995}/g));
		}

		// Iterate through all the shorter lines and concat them into larger message-sized
		// properly formatted chunks.
		var chunkedResponse = [],
			chunk = "",
			line = "";

		for (var i = 0; i < chunkedResponseLines.length; i++) {
			line = chunkedResponseLines[i];

			if((chunk.length + line.length) > 1995){
				chunkedResponse.push(chunk);
				chunk = line;
			} else {
				chunk += "\n\n" + line;
			}
		}

		// Iterate through each message sized chunk and reply
		for (var i = 0, l = chunkedResponse.length; i < l; i++) {
			e.message.reply(chunkedResponse[i].trim());
		}

	}
}

function addEmote(e){
	function storeEmote(attachment){
		emotesList.cacheEmote(attachment, () => {
			e.message.reply("Emote " + attachment.filename + " was cached");
		}, () => {
			e.message.reply("Emote " + attachment.filename + " could not be cached");
		});
	}

	for (var i = 0, l = e.message.attachments.length; i < l; i++) {
		storeEmote(e.message.attachments[i]);
	}
}

function rmvEmote(e){	
	var commandParam = e.message.content.substring("rmvemote ".length);
	if(emotesList.removeEmote(commandParam)){
		e.message.reply("Removed emote "+commandParam);
	} else {
		e.message.reply("Couldn't find emote "+commandParam);
	}
}

// Misc bot Client stuff
function setGame(e){
	var commandParam = e.message.content.substring("game ".length);

	if(commandParam.length){
		client.User.setGame({ name: commandParam });
		e.message.reply("Game set to "+commandParam);
	} else {
		client.User.setGame(null);
		e.message.reply("Game set to nothing");
	}
}

var request = require('request');
function setAvatar(e){
	if(e.message.attachments.length){
		var imgParts = [],
			errors = false;

		request(e.message.attachments[0].url)
			.on('error', () => { errors = true; })
			.on('data', part => { imgParts.push(part); })
			.on('end', () => {
				if(errors){
					e.message.reply('There was an error...');					
				} else {
					client.User.setAvatar(Buffer.concat(imgParts));
					e.message.reply('Successfully updated avatar');
				}
			});
	} else {
		client.User.setAvatar(null);
		e.message.reply('Successfully updated avatar to nothing');
	}
}

// Exports
module.exports = {
	client: client,
};