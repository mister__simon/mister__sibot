module.exports = {
	owner: process.env.BOT_OWNER_NAME,
	bot_short_name: process.env.BOT_NAME_SHORT,
	token: process.env.BOT_TOKEN,
	emotes_cache: './mister__sibot/emotes/cache'
}