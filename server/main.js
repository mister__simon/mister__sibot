var express = require('express'),
	path = require('path'),
	open = require('open'),
	style = require('../lib/logStyles');

var app = express(),
	location = path.join(__dirname, 'web'),
	port = process.env.PORT || 8888;


// Todo... Use a templating engine instead of AJAXing static html pages.
app.get('/bot/invite', (req, res) => {
	var inviteLink = 'https://discordapp.com/oauth2/authorize?client_id=';
		inviteLink += process.env.BOT_ID;
		inviteLink += '&scope=bot&permissions=0';

	res.send(inviteLink);
});

app.get('/bot/name', (req, res) => {
	res.send(process.env.BOT_NAME_LONG);
});

app.use('/', express.static(location));

app.listen(port, () => {
	console.log(style.success("Server is listening"));

	if(process.env.AUTO_OPEN_UI == 1){
		open("http://localhost:"+port);
	}
});

module.exports = app;