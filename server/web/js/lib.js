(function(){
	function onResponseReady(req, onSuccess, onError){
		if (req.readyState === XMLHttpRequest.DONE) {
			if(req.status === 200){
				onSuccess(req);					
			} else {
				onError(req);
			}
		}
	}

	function getData(location, method, onSuccess, onError){
		var httpRequest = new XMLHttpRequest();
		httpRequest.onreadystatechange = function(){
			onResponseReady(httpRequest, onSuccess, onError);
		};
		httpRequest.open(method, location);
		httpRequest.send();
	}

	window.getData = getData;
})();