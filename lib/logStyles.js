var chalk = require('chalk');

module.exports = {
	welcome: chalk.dim.gray,
	success: chalk.bold.green,
	error: chalk.bold.red,
	chatMainIn: chalk.bold.cyan,
	chatMainOut: chalk.bold.magenta,
	chatIn: chalk.cyan,
	chatOut: chalk.magenta
};